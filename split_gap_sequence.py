def split_gap_sequence(x, interval=1):
    d = x[1:]-x[:-1]
    end = np.r_[np.where(d > interval)[0], len(x)-1]
    beg = np.r_[0, end[:-1]+1]
    split = []
    for be in zip(beg,end):
        split.append(x[be[0]:be[1]+1])
    return split
